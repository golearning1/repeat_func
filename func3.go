     //Variadic function

package main
 
import(
    "fmt"
    "strings"
)
 
func main() {
    
	element:= []string{"geeks", "FOR", "geeks"}
	fmt.Println(joinstr(element...))
 }
 
func joinstr(element...string)string{
    return strings.Join(element, "-")
}
 
